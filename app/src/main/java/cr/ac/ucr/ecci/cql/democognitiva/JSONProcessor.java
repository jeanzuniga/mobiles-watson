package cr.ac.ucr.ecci.cql.democognitiva;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Jean Carlo on 02/06/2017.
 */

public class JSONProcessor  extends AsyncTask<String, Void, ArrayList> {

    //region variables

    private String FinalJSON;
    private ArrayList<Double> Values = new ArrayList<>();
    public JSONProcessor( String finalJson) {
        this.FinalJSON = finalJson;
    }

    //endregion

    //region class methods

    public void JSONProcessor() {
        try {
            JSONObject obj = new JSONObject(FinalJSON);

            Object w3 = obj.get("document_tone");

            obj = new JSONObject(w3.toString());
            w3 = obj.get("tone_categories");

            FinalJSON = w3.toString();
            FinalJSON = FinalJSON.substring(0, FinalJSON.indexOf(",\"category_name\":"));
            FinalJSON = FinalJSON + "}]";

            JSONArray arr = new JSONArray(FinalJSON);
            JSONObject jObj = arr.getJSONObject(0);

            while (FinalJSON.indexOf("score") != -1) {
                String score = FinalJSON.substring(FinalJSON.indexOf("score") + 7, FinalJSON.length() - 1);
                String scoreValor =
                        score.substring(0, score.indexOf(","));
                FinalJSON = FinalJSON.substring(FinalJSON.indexOf("score") + 8, FinalJSON.length() - 1);
                Values.add(Double.parseDouble(scoreValor));
            }
        }
        catch (Throwable t)
        {
            Log.e("My App", "Could not parse malformed JSON: \"" + FinalJSON + "\"");
        }
    }

    public ArrayList GetValues()
    {
        return this.Values;
    }

    //endregion

    //region async methods

    @Override
    protected ArrayList doInBackground(String... params) {
        JSONProcessor();
        return GetValues();
    }

    //endregion
}
