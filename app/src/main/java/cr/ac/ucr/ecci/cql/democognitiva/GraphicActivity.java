package cr.ac.ucr.ecci.cql.democognitiva;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Andreina on 16/06/2017.
 */

public class GraphicActivity extends AppCompatActivity {

    //region variables

    private String Perceptions;
    private String ProcessedText;
    private boolean Translate;
    TextView TextoProgreso;
    ProgressBar BarraProgreso;

    //endregion

    //region activity methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graphic);

        Intent intent = getIntent();
        ProcessedText = intent.getStringExtra("ProcessedText");
        Translate = intent.getBooleanExtra("Translate", true);
        TextoProgreso = (TextView) findViewById(R.id.textoProgreso);
        BarraProgreso = (ProgressBar) findViewById(R.id.barraProgreso);

        new Connection(this, ProcessedText).execute();
    }

    public void GenerateGraphic() {
        ArrayList<BarEntry> entries = new ArrayList<>();
        Perceptions = Perceptions.substring(1, Perceptions.length() - 2);
        List<String> elephantList = Arrays.asList(Perceptions.split(","));

        for (int i = 0; i < elephantList.size(); i++) {
            float a = Float.parseFloat(elephantList.get(i));
            entries.add(new BarEntry(a, i));
        }

        BarDataSet dataset = new BarDataSet(entries, "Valor de la emoción");

        ArrayList<String> labels = new ArrayList<String>();
        labels.add("Enojo");
        labels.add("Disgusto");
        labels.add("Miedo");
        labels.add("Alegría");
        labels.add("Tristeza");

        BarChart chart = new BarChart(this);
        setContentView(chart);

        BarData data = new BarData(labels, dataset);
        chart.setData(data);

        chart.setDescription("Percepción obtenida:");
    }

    //endregion

    //region async class to get analysis

    private class Connection extends AsyncTask<String, Float, String> {
        GraphicActivity context;
        private String Perceptions;
        private String ProcessedText;

        public Connection(GraphicActivity context, String ProcessedText){
            this.context = context;
            this.ProcessedText = ProcessedText;
        }

        @Override
        protected void onPreExecute() {
            TextoProgreso.setVisibility(View.VISIBLE);
            BarraProgreso.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;
            String userTranslatePassword = "a6c9a2f5-f6b1-4a22-abc9-f4b335bf2b5c" + ":" + "p83I0fepyTBp";
            String userTonePassword = "d15bc900-3f56-4635-90ba-158ca672b355" + ":" + "6DPogAn6D1dN";

            try {
                if(this.context.Translate == true){
                    //Conexion para el servicio de traduccion
                    URL translationURL = new URL("https://gateway.watsonplatform.net/language-translator/api/v2/translate");
                    byte[] postDataBytes = ConvertToJSON().getBytes("UTF-8");
                    connection = (HttpURLConnection) translationURL.openConnection();
                    if(connection != null){
                        String translatedText = ConnectToService(connection, userTranslatePassword, postDataBytes);
                        translatedText = translatedText.replaceAll("%20", " ");
                        translatedText = translatedText.replaceAll("\n", "%20");

                        //Conexion para el servicio de tonos
                        URL watsonURL = new URL("https://gateway.watsonplatform.net/tone-analyzer/api/v3/tone?version=2016-05-19");
                        postDataBytes = GetPerceptionsJSON(RefineTranslation(translatedText)).getBytes("UTF-8");
                        connection = (HttpURLConnection) watsonURL.openConnection();
                        if(connection != null){
                            String finalJson = ConnectToService(connection, userTonePassword, postDataBytes);

                            //Procesamos el Json
                            JSONProcessor jsonProcessor = new JSONProcessor(finalJson);
                            jsonProcessor.JSONProcessor();
                            this.Perceptions = jsonProcessor.GetValues().toString();
                        }
                        else{
                            // Can not connect to watson
                            this.Perceptions = "watston";
                        }
                    }
                    else{
                        // Can not connect to translator
                        this.Perceptions = "translator";
                    }
                }
                else{
                    //Conexion para el servicio de tonos
                    URL watsonURL = new URL("https://gateway.watsonplatform.net/tone-analyzer/api/v3/tone?version=2016-05-19");
                    byte[] postDataBytes = GetPerceptionsJSON(this.ProcessedText).getBytes("UTF-8");
                    connection = (HttpURLConnection) watsonURL.openConnection();
                    if(connection != null){
                        String finalJson = ConnectToService(connection, userTonePassword, postDataBytes);

                        //Procesamos el Json
                        JSONProcessor jsonProcessor = new JSONProcessor(finalJson);
                        jsonProcessor.JSONProcessor();
                        this.Perceptions = jsonProcessor.GetValues().toString();
                    }
                    else{
                        // Can not connect to watson
                        this.Perceptions = "watston";
                    }
                }
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            finally {
                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            this.context.Perceptions = this.Perceptions;
            return this.Perceptions;
        }

        @Override
        protected void onPostExecute(String string) {
            TextoProgreso.setVisibility(View.INVISIBLE);
            BarraProgreso.setVisibility(View.INVISIBLE);
            this.context.GenerateGraphic();
        }

        public String ConnectToService(HttpURLConnection connection,String password, byte[] postDataBytes) {
            String encrypedPwd;
            InputStream stream;
            StringBuffer buffer = null;
            String line;
            BufferedReader reader;

            try {
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                encrypedPwd = Base64.encodeToString(password.getBytes("UTF-8"), Base64.DEFAULT);
                connection.setRequestProperty("Authorization", "Basic " + encrypedPwd);
                connection.getOutputStream().write(postDataBytes);
                connection.connect();
                stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));
                buffer = new StringBuffer();
                line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return buffer.toString();
        }

        private String ConvertToJSON() {
            String JSON = "";

            JSON = "{\n" +
                    "\t\"text\": \"" + this.ProcessedText + "\",\n" +
                    "\t\"source\": \"es\",\n" +
                    "\t\"target\": \"en\"\n" +
                    "}";

            return JSON;
        }

        private String GetPerceptionsJSON(String text) {
            String JSON = "";
            JSON = "{\n" +
                    "\t\"text\": \"" + text + "\"\n" +
                    "}";

            return JSON;
        }

        private String RefineTranslation(String translatedText) {
            translatedText = translatedText.substring(translatedText.indexOf("\"translation\":\"") + 15);
            String refinedTranslation;
            refinedTranslation = translatedText.substring(0, translatedText.indexOf("\""));

            return refinedTranslation;
        }
    }

    //endregion
}
