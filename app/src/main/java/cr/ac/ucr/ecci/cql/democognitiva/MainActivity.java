package cr.ac.ucr.ecci.cql.democognitiva;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

/**
 * Created by Jean Carlo on 02/06/2017.
 */

public class MainActivity extends AppCompatActivity {

    //region variables

    private Button ButtonAnalyze;
    private EditText EntryText;
    private String Perceptions;
    private String ProcessedText;
    private boolean Translate;

    //endregion

    //region activity methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButtonAnalyze = (Button) findViewById(R.id.ButtonAnalyze);
        EntryText = (EditText) findViewById(R.id.EntryText);

        ButtonAnalyze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();

            if(activeNetwork != null) {
                if (!(activeNetwork.isConnectedOrConnecting())) {
                    Toast.makeText(MainActivity.this, "No hay conexión a internet. ¡Intente más tarde!", Toast.LENGTH_SHORT).show();
                } else {
                    ProcessedText = EntryText.getText().toString();
                    ProcessedText = ProcessedText.replaceAll(" ", "%20");
                    ProcessedText = ProcessedText.replaceAll("\n", "%20");

                    GoToGraffic();
                }
            }
            else{
                Toast.makeText(MainActivity.this, "No hay conexión a internet. ¡Intente más tarde!", Toast.LENGTH_SHORT).show();
            }
            }

        });
    }

    @Override
    public void onRestart(){
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    public void onRadioButtonClicked(View view) {
        // Verificar el RadioButton seleccionado
        boolean checked = ((RadioButton) view).isChecked();
        // Determinar cual RadioButton esta seleccionado
        switch(view.getId()) {
            case R.id.radioButtonEnglish:
                if (checked)
                    Translate = false;
                break;
            case R.id.radioButtonSpanish:
                if (checked)
                    Translate = true;
                break;
        }
    }

    private void GoToGraffic() {
        Intent intent = new Intent(this, GraphicActivity.class);
        intent.putExtra("ProcessedText", ProcessedText);
        intent.putExtra("Translate", Translate);
        startActivity(intent);
    }

    //endregion
}